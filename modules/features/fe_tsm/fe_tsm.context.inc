<?php
/**
 * @file
 * fe_tsm.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function fe_tsm_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_issue_tour';
  $context->description = '';
  $context->tag = 'techsupport';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/project-issue/*' => 'node/add/project-issue/*',
      ),
    ),
  );
  $context->reactions = array(
    'joyride_add' => array(
      'joyride_auto_start' => 1,
      'joyride_play_once' => 1,
      'joyride_tour_content' => '<li data-class="form-item-title input" data-button="Next" data-options="tipLocation:right">
  <h3>Issue Metadata</h3>
  <p>Issue metadata describe details about your issue</p>
</li>
<li data-class="form-item-field-issue-parent-und-0-target-id input" data-button="Next" data-options="tipLocation:right">
  <h3>Issue relationships</h3>
  <p>If your issue have parent issue or similar like other issue then you may relate that issue here</p>
</li>
<li data-class="form-item-field-issue-files-und-0" data-button="Close">
  <h3>Issue Screenshot</h3>
  <p>Do you have screenshot for your issue then upload it here.</p>
</li>',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('techsupport');
  $export['create_issue_tour'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_project_tour';
  $context->description = '';
  $context->tag = 'techsupport';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/project' => 'node/add/project',
      ),
    ),
  );
  $context->reactions = array(
    'joyride_add' => array(
      'joyride_auto_start' => 1,
      'joyride_play_once' => 1,
      'joyride_tour_content' => '<li data-class="form-item-title input" data-button="Next" data-options="tipLocation:left">
  <h3>Create Project</h3>
  <p>Create your first project to track your issues</p>
</li>
<li data-id="field-project-components-values" data-button="Close" data-options="tipLocation:top">
  <h3>Components</h3>
  <p>Default components of projects. You may create more based on your need.</p>
</li>',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('techsupport');
  $export['create_project_tour'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'techsupport_tour';
  $context->description = '';
  $context->tag = 'techsupport';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'joyride_add' => array(
      'joyride_auto_start' => 1,
      'joyride_play_once' => 1,
      'joyride_tour_content' => '<li data-class="sitename" data-button="Next" data-options="tipLocation:left">
  <h3>Logo and Site Name</h3>
  <p>Easily customizable logo and site name for your projects.</p>
</li>
<li data-id="block-system-navigation" data-button="Next" data-options="tipLocation:left">
  <h3>Navigation Menu</h3>
  <p>Cool navigation menu for everything. This area also customizable by administrator.</p>
</li>
<li data-class="views-widget-filter-field_issue_status_value" data-button="Close" data-options="tipLocation:top">
  <h3>Issues for all projects</h3>
  <p>Here you can see all issues reported by the customers.</p>
</li>',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('techsupport');
  $export['techsupport_tour'] = $context;

  return $export;
}
