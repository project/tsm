<?php
/**
 * @file
 * fe_tsm.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fe_tsm_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function fe_tsm_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function fe_tsm_node_info() {
  $items = array(
    'project' => array(
      'name' => t('Project'),
      'base' => 'node_content',
      'description' => t('Content type for project management.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'project_issue' => array(
      'name' => t('Issue'),
      'base' => 'node_content',
      'description' => t('An issue that can be tracked, such as a bug report, feature request, or task.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
