<?php
/**
 * @file
 * fe_tsm.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function fe_tsm_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access user profiles'.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: 'administer projects'.
  $permissions['administer projects'] = array(
    'name' => 'administer projects',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project',
  );

  // Exported permission: 'assign and be assigned project issues'.
  $permissions['assign and be assigned project issues'] = array(
    'name' => 'assign and be assigned project issues',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project_issue',
  );

  // Exported permission: 'create full project projects'.
  $permissions['create full project projects'] = array(
    'name' => 'create full project projects',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project',
  );

  // Exported permission: 'create full projects'.
  $permissions['create full projects'] = array(
    'name' => 'create full projects',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project',
  );

  // Exported permission: 'create project content'.
  $permissions['create project content'] = array(
    'name' => 'create project content',
    'roles' => array(
      'administrator' => 'administrator',
      'associate' => 'associate',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create project_issue content'.
  $permissions['create project_issue content'] = array(
    'name' => 'create project_issue content',
    'roles' => array(
      'administrator' => 'administrator',
      'associate' => 'associate',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create sandbox projects'.
  $permissions['create sandbox projects'] = array(
    'name' => 'create sandbox projects',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project',
  );

  // Exported permission: 'delete any full project'.
  $permissions['delete any full project'] = array(
    'name' => 'delete any full project',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project',
  );

  // Exported permission: 'delete any project content'.
  $permissions['delete any project content'] = array(
    'name' => 'delete any project content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any project_issue content'.
  $permissions['delete any project_issue content'] = array(
    'name' => 'delete any project_issue content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any sandbox project'.
  $permissions['delete any sandbox project'] = array(
    'name' => 'delete any sandbox project',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project',
  );

  // Exported permission: 'delete own full project'.
  $permissions['delete own full project'] = array(
    'name' => 'delete own full project',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project',
  );

  // Exported permission: 'delete own project content'.
  $permissions['delete own project content'] = array(
    'name' => 'delete own project content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own project_issue content'.
  $permissions['delete own project_issue content'] = array(
    'name' => 'delete own project_issue content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own sandbox project'.
  $permissions['delete own sandbox project'] = array(
    'name' => 'delete own sandbox project',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'project',
  );

  // Exported permission: 'edit any project content'.
  $permissions['edit any project content'] = array(
    'name' => 'edit any project content',
    'roles' => array(
      'administrator' => 'administrator',
      'associate' => 'associate',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any project_issue content'.
  $permissions['edit any project_issue content'] = array(
    'name' => 'edit any project_issue content',
    'roles' => array(
      'administrator' => 'administrator',
      'associate' => 'associate',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own project content'.
  $permissions['edit own project content'] = array(
    'name' => 'edit own project content',
    'roles' => array(
      'administrator' => 'administrator',
      'associate' => 'associate',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own project_issue content'.
  $permissions['edit own project_issue content'] = array(
    'name' => 'edit own project_issue content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
